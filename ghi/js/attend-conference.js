window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      var loading_icon = document.getElementById("loading-conference-spinner"); // get loading icon id
      loading_icon.classList.add("d-none");     // add 'd-none' class to loading icon
      selectTag.classList.remove("d-none");     // remove 'd-none' class from select tag

      const formTag = document.getElementById('create-attendee-form');
      const success_msg = document.getElementById('success-message');
      formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAttendee = await response.json();
            formTag.classList.add("d-none");
            success_msg.classList.remove("d-none");
        }
    });
    }

  });
