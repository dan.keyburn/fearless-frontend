window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
          return `
              <div class="alert alert-danger" role="alert">Something went wrong!
              </div>
          `;
        } else {
        const data = await response.json();
        const selectTag = document.getElementById('state');
        for (let state of data.states) {
            // Create an 'option' element
            let option = document.createElement('option');
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = Object.values(state);
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = Object.keys(state);
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
        // State fetching code, here...
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
            }
        });
      }
    } catch (e) {
        // Figure out what to do if an error is raised
        return `
          <div class="alert alert-danger" role="alert">Something went wrong!
          </div>
        `;
      }
  });
